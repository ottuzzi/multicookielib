package org.brucalipto.multicookielib;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Cookie extends javax.servlet.http.Cookie implements Serializable 
{
	public final static int MAX_COOKIE_SIZE_IN_BYTES = 4000;
	
	private static final long serialVersionUID = 7496338846835887035L;
	
	private final boolean encrypted;
	private final List<NameValuePair> pairs = new ArrayList<NameValuePair>();

	public Cookie(String name, String value) 
	{
		this(name, value, false);
	}

	public Cookie(String name, String value, boolean encrypted) 
	{
		super(name, value);
		this.encrypted = encrypted;
		pairs.add(new NameValuePair(name, value));
	}

	public boolean isEncrypted() 
	{
		return encrypted;
	}
	
	public void addNameValue(NameValuePair added)
	{
		pairs.add(added);
	}
}
