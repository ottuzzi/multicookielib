package org.brucalipto.multicookielib;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONValue;

public class MultiCookieManager 
{
	public final static int MAX_COOKIES_PER_DOMAIN = 20;
	
	private final static Log log = LogFactory.getLog(MultiCookieManager.class);
	private final static String COMMON_COOKIES_NAME = "MCM";
	/* 
	 * Max cookie size is reported to be 4000bytes including domain, path, maxAge, etc etc. So 
	 * we stay on the safe side setting i to 3900bytes only for cookie value
	 * 
	 */
	private final static int MAX_COOKIE_VALUE_LENGHT = 3900;
	
	public final String commonCookiesName;
	
	private final int maxCookies;
	private final Map<String, List<Cookie>> cookiesMap = new HashMap<String, List<Cookie>>();
	private final Map<String, Integer> cookiesNamesCounter = new HashMap<String, Integer>();
	
	private int cookieNamesCounter = 0;
	
	
	public MultiCookieManager()
	{
		this(MAX_COOKIES_PER_DOMAIN, COMMON_COOKIES_NAME);
	}
	
	public MultiCookieManager(int maxCookies)
	{
		this(maxCookies, COMMON_COOKIES_NAME);
	}
	
	public MultiCookieManager(int maxCookies, String commonCookiesName)
	{
		this.maxCookies = maxCookies;
		this.commonCookiesName = commonCookiesName;
	}
	
	public void addCookie(Cookie cookie)
	{
		String cookieKey = getCookieKey(cookie);
		log.debug("Going to store cookie with key: '"+cookieKey+"'");
		List<Cookie> cookies = cookiesMap.get(cookieKey);
		if (cookies==null)
		{
			log.debug("Cannot find a list with key: '"+cookieKey+"'");
			cookies = new ArrayList<Cookie>();
			cookiesMap.put(cookieKey, cookies);
			cookiesNamesCounter.put(cookieKey, new Integer(cookieNamesCounter++));
		}
		cookies.add(cookie);
	}
	
	public List<Cookie> getCookies()
	{
		if (cookiesMap.isEmpty())
		{
			return Collections.emptyList();
		}
		
		Set<String> keys = cookiesMap.keySet();
		Iterator<String> keysIter = keys.iterator();
		while (keysIter.hasNext())
		{
			String key = keysIter.next();
			List<Cookie> cookies = cookiesMap.get(key);
			int cookiesSize = cookies.size();
			LinkedList<NameValuePair> nvpList = new LinkedList<NameValuePair>();
			for (int i=0; i<cookiesSize; i++)
			{
				Cookie cookie = cookies.get(i);
				NameValuePair nvp = new NameValuePair(cookie.getName(), cookie.getValue());
				nvpList.add(nvp);
			}
			String finalCookieValue = JSONValue.toJSONString(nvpList);
			log.debug(finalCookieValue.length()+" - "+finalCookieValue);
			String[] result = new Splitter(MAX_COOKIE_VALUE_LENGHT).split(finalCookieValue);
			
		}
		
		return Collections.emptyList();
	}
	
	/**
	 * We want to group cookies with a key constructed with
	 * domain
	 * path
	 * maxAge
	 * version
	 */
	private String getCookieKey(Cookie cookie)
	{
		return cookie.getDomain()+"_"+cookie.getPath()+"_"+cookie.getMaxAge()+"_"+cookie.getVersion();
	}
}
