package org.brucalipto.multicookielib;

import java.io.Serializable;

public class NameValuePair implements Serializable 
{
	private static final long serialVersionUID = -8033873922497857330L;
	
	public final String name;
	public final String value;
	
	public NameValuePair(String name, String value)
	{
		this.name = name;
		this.value = value;
	}
}
